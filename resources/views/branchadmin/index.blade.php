@extends('layouts.app')


@section('content')
<div class="container">

    <div id="page-wrapper">
            <div class="container-fluid">
              
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">Bütün İstifadəçilər</h3>
                           
                            <div class="table-responsive">
                                <table class="table color-table primary-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Ad</th>
                                            <th>Soyad</th>
                                            <th>Elektron Poçt</th>
                                            <th>Vəzifəsi</th>
                                            <th>Status</th>


<td>
<a href="/sertified/{{$user->id}}" class="btn btn-info">
    Sertifkat
</a>
</td>
                                            <th></th>
                                            <th></th>



                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($users as $user)

                                        <tr>
                                        @if($user->type!=6 and  $user->type!=0)
                                        
                                            <td>{{$user->id}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->surname}}</td>

                                            <td>{{$user->email}}</td>
                                            

                                            <td>
                                            @if($user->type==1)
                                            <kbd>Tələbə</kbd>
                                            @endif

                                            @if($user->type==2)
                                            <kbd>
                                            Valideyn
                                            </kbd>
                                            @endif

                                            @if($user->type==6)
                                            <kbd>Filial Rehberi</kbd>
                                            @endif

                                                @if($user->type==7)
                                            <kbd>Muellim</kbd>
                                            @endif

                                            @if($user->type==3)
                                            <kbd>Reseption</kbd>
                                            @endif

                                            
                                            </td>
                                          <td>
                                            <a class="btn btn-warning" href="/users/{{$user->id}}/edit/">
                                            Redakte Et
                                            </a>
                                            </td>
                                            <td>
                                   <form method="POST" action="/users/{{$user->id}}/">
                                   {{csrf_field()}}
    <input type="hidden" name="_method" value="DELETE" />
    <button type="submit" class="btn btn-danger">Sil</button>

</form>
</td>   
                                        </tr>
                                        @endif
                                       
                                            @endforeach
                                 	
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                   
                </div>


@stop