@extends('layouts.app')

@section('content')
<div class="container-fluid">
<div class="row">
    <div class="col-lg-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Sistemdəki mesajlar</h3>
            <!-- <p class="text-muted m-b-20">Create responsive tables by wrapping any <code>.table</code> in <code>.table-responsive </code></p> -->
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Göndərən</th>
                            <th>Göndərilən</th>
                            <th>Mesajın təsviri</th>
                            <th>Status</th>
                            <th>Tarixi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $c)
                        <tr>
                            <td>{{$c->user->name.' '.$c->user->surname}}</td>
                            <td>{{$c->user_to->name.' '.$c->user_to->surname}}</td>
                            <td><a href="{{url('/single-message/'.$c->id)}}">{{str_limit($c->message,40)}}</a></td>
                            <td>@if($c->status == 1)
                                <div class="label label-table label-success">Oxundu</div>
                              @else
                                <div class="label label-table label-danger">Oxunmayıb</div>
                              @endif
                            </td>
                            <td>{{$c->created_at}}</td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
@stop
