@extends('layouts.app') @section('content')
<style type="text/css">
    label {
        right: 1vw;
    }
</style>

<div class="container">

    <div class="row">

        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
              @if ($message = Session::get('success'))
              <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
              </div>
              @endif
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('users.update', $user->id)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="PATCH">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="col-md-12">Ad</label>

                                    <input placeholder="Ad" value="{{$user->name}}" name="name" type="text" class="form-control form-control-line"> </div>

                                <!-- user -->
                                <div class="col-md-6">
                                    <label class="col-md-12">Soyad</label>

                                    <input placeholder="Soyad" value="{{$user->surname}}" name="surname" type="text" class="form-control form-control-line"> </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="col-md-12">Email</label>

                                    <input placeholder="e-mail" value="{{$user->email}}" type="email" id="example-email2" name="email" class="form-control form-control-line"> </div>

                                <!-- user -->
                                <div class="col-md-6">
                                    <label class="col-md-12">Şifrə </label>

                                    <input name="password" type="password" class="form-control form-control-line"> </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="col-md-12">Mobil No</label>

                                    <input placeholder="Mobil No" value="{{$user->mobile}}" name="mobile" type="text" class="form-control form-control-line"> </div>
                                <div class="col-md-6">
                                    <label class="col-sm-6">Vəzifə</label>

                                    <select name="type" id="vezife" name="type" class="form-control form-control-line">
                                        <option disabled selected value="">-- Seçin --</option>
                                        @if(Auth::user()->type==6)
                                        <option {{$user->type == 3 ? 'selected' : ''}} value="3">Reseption</option>
                                        <option {{$user->type == 7 ? 'selected' : ''}} value="7">Müəllim</option>
                                        <option {{$user->type == 1 ? 'selected' : ''}} value="1">Tələbə</option>
                                        <option {{$user->type == 2 ? 'selected' : ''}} value="2">Valideyin</option>
                                        <option {{$user->type == 5 ? 'selected' : ''}} value="5">SMM</option>
                                        @endif @if(Auth::user()->type==0)
                                        <option {{$user->type == 6 ? 'selected' : ''}} value="6">Filial Rəhbəri</option>
                                        <option {{$user->type == 3 ? 'selected' : ''}} value="3">Reseption</option>
                                        <option {{$user->type == 7 ? 'selected' : ''}} value="7">Müəllim</option>
                                        <option {{$user->type == 1 ? 'selected' : ''}} value="1">Tələbə</option>
                                        <option {{$user->type == 2 ? 'selected' : ''}} value="2">Valideyin</option>
                                        <option {{$user->type == 5 ? 'selected' : ''}} value="5">SMM</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="col-sm-12">Filial</label>
                            <div style="padding-left: 0px;" class="col-md-12">
                                @if(Auth::user()->type==0)

                                <select id="filial" name="branch_id" class="form-control form-control-line">

                                    @foreach($branch as $branc)
                                    <option {{$user->branch_id == $branc->id ? 'selected' : ''}} value="{{$branc->id}}">{{$branc->name}}</option>
                                    @endforeach
                                </select>
                                @else
                                <select id="filial" name="branch_id" class="form-control form-control-line">

                                    <option  value="{{Auth::user()->branch_id}}">{{Auth::user()->branch->name}}</option>

                                </select>
                                @endif

                                <select id="qurup" style="display: none;" name="group_id" class="form-control form-control-line">
                                    @if(Auth::user()->type==0) @foreach($branch->all() as $branc)
                                    <optgroup label="{{$branc->name}}">
                                        @foreach($branc->group->all() as $grop)
                                        <option value="{{$grop->id}}">{{$grop->number}}</option>
                                        @endforeach
                                    </optgroup>
                                    @endforeach @else @foreach($gro as $grou)
                                    <option value="{{$grou->id}}">{{$grou->number}}</option>
                                    @endforeach @endif

                                </select>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <label class="col-sm-12">Doğum Tarixi </label>
                            <div style="padding-left: 0px;" class="col-sm-12">
                                <input type="date" value="{{$user->date}}" name="date" class="form-control form-control-line" placeholder="Helping text">
                                <span class="help-block">
                                     <small>Məlumatları Yadda Saxlamaq Üçün Göndər  Düyməsini Sıx.</small></span> </div>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-sm-6">
                            <label>Şəkil Yüklə</label>

                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control form-control-line" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new"> Foto </span> <span class="fileinput-exists">Dəyiş</span>
                                <input type="file" name="img"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Sil</a> </div>
                        </div>

                        <div class="col-md-6">
                            <label>Haqqında</label>
                            <textarea style="height: 3.5vw;" name="about" class="form-control form-control-line">
                                {{$user->about}}
                            </textarea>
                            <br>
                        </div>
                    </div>
                    <div>
                        @if($user->img!=null)
                        <img src="/uploads/{{$user->img}}" width="180" class="img-responsive"> @else
                        <img src="/uploads/default.png" class="img-responsive" width="180"> @endif

                        <div class="form-group">

                            <div class="col-sm-4"></div>

                            <div class="col-sm-4">
                                <input type="submit" value="Göndər" class="btn btn-block btn-outline btn-info">
                            </div>
                        </div>

                    </div>

                    </form>

                    </form>

                </div>

            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    // $(document).ready(function(){
    $("#vezife").click(function() {
        $data = $("#vezife").val();
        // });

        if ($data == 7 || $data == 1 || $data == 2) {
            $("#filial").hide();

            $("#qurup").show();
            // alert($data);
        } else {
            $("#filial").show();

            $("#qurup").hide();

        }
    });
</script>
<!-- ent layout -->

@endsection
