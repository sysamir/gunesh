@extends('layouts.app') @section('content')
<style type="text/css">
    label {
        right: 1vw;
    }
</style>

<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
              @if ($message = Session::get('success'))
              <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
              </div>
              @endif
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{url('/users')}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="col-md-12">Ad</label>

                                    <input placeholder="Ad" name="name" type="text" class="form-control form-control-line"> </div>

                                <!-- user -->
                                <div class="col-md-6">
                                    <label class="col-md-12">Soyad</label>

                                    <input placeholder="Soyad" name="surname" type="text" class="form-control form-control-line"> </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="col-md-12">E-Poçt</label>

                                    <input placeholder="e-mail" type="email" id="example-email2" name="email" class="form-control form-control-line"> </div>

                                <!-- user -->
                                <div class="col-md-6">
                                    <label class="col-md-12">Şifrə</label>

                                    <input name="password" type="password" class="form-control form-control-line"> </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="col-md-12">Telefon</label>

                                    <input placeholder="Mobil No" name="mobile" type="number" class="form-control form-control-line"> </div>
                                <div class="col-md-6">
                                    <label class="col-sm-6">Vəzifə</label>
                                    <div style="padding-left: 0px;" style="padding-left: 0px;" class="col-sm-12">
                                        <select id="vezife" name="type" class="form-control form-control-line">

                                            @if(Auth::user()->type==6)
                                            <option value="3">Reseption</option>
                                            <option value="7">Müəllim</option>
                                            <option value="1">Tələbə</option>
                                            <option value="2">Valideyin</option>
                                            @endif @if(Auth::user()->type==0)
                                            <option value="6">Filial Rəhbəri</option>
                                            <option value="3">Reseption</option>
                                            <option value="7">Müəllim</option>
                                            <option value="1">Tələbə</option>
                                            <option value="2">Valideyin</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6">
                                    <label id="label_filial" class="col-sm-12">Filial</label>
                                    <label style="display: none" id="label_qrup"  class="col-sm-12">Qrup</label>
                                    <div style="padding-left: 0px;" class="col-md-12">
                                        @if(Auth::user()->type==0)
                                        <select id="filial" name="branch_id" class="form-control form-control-line">
                                            @foreach($branch as $branc)
                                            <option value="{{$branc->id}}">{{$branc->name}}</option>
                                            @endforeach
                                        </select>
                                        @else
                                        <select id="filial" name="branch_id" class="form-control form-control-line">
                                            <option value="{{Auth::user()->branch_id}}">{{Auth::user()->branch->name}}</option>
                                        </select>
                                        @endif

                                        <select id="qurup" style="display: none;" name="group_id" class="form-control form-control-line">
                                            @if(Auth::user()->type==0) @foreach($branch->all() as $branc)
                                            <optgroup label="{{$branc->name}}">
                                                @foreach($branc->group->all() as $grop)
                                                <option value="{{$grop->id}}">{{$grop->number}}</option>
                                                @endforeach
                                            </optgroup>
                                            @endforeach @else @foreach($gro as $grou)
                                            <option value="{{$grou->id}}">{{$grou->number}}</option>
                                            @endforeach @endif
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <label class="col-sm-12">Doğum Tarixi </label>
                                    <div style="padding-left: 0px;" class="col-sm-12">
                                        <input type="date" name="date" class="form-control form-control-line" placeholder="Helping text">
                                        <span class="help-block">
                                     </span> </div>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-sm-6">
                                    <label>Şəkil Yüklə</label>

                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control form-control-line" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new"> Foto </span> <span class="fileinput-exists">Dəyiş</span>
                                        <input type="file" name="img" required> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Sil</a> </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Haqqında</label>
                                    <textarea style="height: 2.5vw;" name="about" class="form-control form-control-line">

                                    </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4"></div>

                                <div class="col-sm-4">
                                    <input type="submit" value="Göndər" class="btn btn-block btn-outline btn-info">
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ent layout -->

    <script type="text/javascript">
        // $(document).ready(function(){
        $("#vezife").click(function() {
            $data = $("#vezife").val();
            // });

            if ($data == 7 || $data == 1 || $data == 2) {
                $("#filial").hide();
                $("#label_filial").hide();
                $("#qurup").show();
                $("#label_qrup").show();
                // alert($data);
            } else {
                $("#filial").show();
                $("#label_filial").show();
                $("#qurup").hide();
                $("#label_qrup").hide();
            }
        });
    </script>
    @endsection
