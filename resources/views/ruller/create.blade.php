@extends('layouts.app')


@section('content')

<div class="container">

<div class="row">
<div class="col-sm-2"></div>

<div class="col-sm-8">
<div class="white-box p-l-20 p-r-20">
<h3 class="box-title m-b-0"></h3>

<div class="row">
  <div class="col-md-12">
      <form action="{{url('/ruller')}}"  method="POST" class="form-horizontal">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

          </div>
          <div class="form-group">
          <label class="col-sm-12">1.Gün dərs Saatı</label>
              <div class="col-md-9">
                  <input name="day1"  type="time" class="form-control form-control-line" value="" placeholder="Dərs Saatı"> </div>
          </div>

          <div class="form-group">
          <label class="col-sm-12">2.Gün dərs Saatı</label>
              <div class="col-md-9">
                  <input name="day2"  type="time" class="form-control form-control-line" value="" placeholder="Dərs Saatı"> </div>
          </div>

          <div class="form-group">
          <label class="col-sm-12">3.Gün dərs Saatı</label>
              <div class="col-md-9">
                  <input name="day3"  type="time" class="form-control form-control-line" value="" placeholder="Dərs Saatı"> </div>
          </div>

          <div class="form-group">
          <label class="col-sm-12">4.Gün dərs Saatı</label>
              <div class="col-md-9">
                  <input name="day4"  type="time" class="form-control form-control-line" value="" placeholder="Dərs Saatı"> </div>
          </div>

          <div class="form-group">
          <label class="col-sm-12">5.Gün dərs Saatı</label>
              <div class="col-md-9">
                  <input name="day5"  type="time" class="form-control form-control-line" value="" placeholder="Dərs Saatı"> </div>
          </div>

          <div class="form-group">
          <label class="col-sm-12">6.Gün dərs Saatı</label>
              <div class="col-md-9">
                  <input name="day6"  type="time" class="form-control form-control-line" value="" placeholder="Dərs Saatı"> </div>
          </div>

          <div class="form-group">
          <label class="col-sm-12">Bazar günü dərs Saatı</label>
              <div class="col-md-9">
                  <input name="day7"  type="time" class="form-control form-control-line" value="" placeholder="Dərs Saatı"> </div>
          </div>




           <div class="form-group">
              <label class="col-sm-12">Qrup</label>
              <div class="col-sm-9">
                  <select name="group_id" class="form-control form-control-line">

                      @foreach($group as $grup )
                      <option value="{{$grup->id}}" >{{$grup->number}}</option>
                      @endforeach
                  </select>
              </div>
          </div>


          <div class="form-group">
              <div class="col-sm-4"></div>
              <div class="col-sm-4">
              <br>
                <input style="margin-top: 1vw;" type="submit" value="Göndər" class="btn btn-block btn-outline btn-info"  >
              </div>
          </div>
        </form>
      </div>
                    <br>
                @if ($message = Session::get('success'))
                  <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                          <strong>{{ $message }}</strong>
                  </div>
                @endif

                @if ($message = Session::get('unsuccess'))
                  <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                          <strong>{{ $message }}</strong>
                  </div>
                @endif

                </div>
            </div>
        </div>
    </div>
</div>


<!-- ent layout -->

@endsection
