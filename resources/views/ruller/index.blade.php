@extends('layouts.app')


@section('content')
<!-- start -->

<div class="container">

    <div id="page-wrapper">
            
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Sistemdə Olan Bütün Cədvəllər </h3>
               
                <div class="table-responsive">
                    <table class="table color-table primary-table">
                        <thead>
                            <tr>
                                
                                <th>#</th>
                                <th>Müəllim Adı</th>
                                <th>Mövzu</th>
                                <th>Tarix</th>
                                <th>Saat</th>
                                


                               <th><i class="fa fa-edit" aria-hidden="true"></i></th>
                                
                               <th><i class="fa fa-trash-o" aria-hidden="true"></i></th>

                      

                            </tr>
                        </thead>
                        <tbody>

                        
                        @foreach ($rullers as $ruller)
                            <tr>
                         
                                <td>{{$ruller->id}}</td>
                                <td>{{$ruller->teacher}}</td>
                                <td>{{$ruller->lesson->name}}</td>
                                <td>{{$ruller->date}}</td>
                                <td>{{$ruller->time}}</td>


                              @if(Auth::user()->type==3 or Auth::user()->type==6 or Auth::user()->type==0 )
                             
                                <td><a class="btn btn-warning" href="/ruller/{{$ruller->id}}/edit">Redaktə Et</a></td>

                             <td>

                                   <form method="POST" action="/ruller/{{$ruller->id}}/">
                                   {{csrf_field()}}
    <input type="hidden" name="_method" value="DELETE" />
    <button type="submit" class="btn btn-danger">Sil</button>

</form>
</td>  
                              @endif
        
                              @if(Auth::user()->type==7)
                              <td><a class="btn btn-info" href="/attendance/{{$ruller->id}}"> Davamiyyet</a> </td>
                              @endif
                              @if(Auth::user()->type==3)
                              <td><a class="btn btn-info" href="/teachers/{{$ruller->id}}"> Davamiyyet</a> </td>
                              @endif



                               

                                
                             
                            
                            </tr>
                            @endforeach


                          
                     	
                        </tbody>
                    </table>
                     @if ($message = Session::get('delete'))
                  <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                          <strong>{{ $message }}</strong>
                  </div>
                @endif
                </div>
            </div>
        </div>
        
       
    </div>




<!-- endlay out -->
@stop