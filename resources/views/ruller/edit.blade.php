@extends('layouts.app')


@section('content')

<div class="container">

  <div class="row">
                    <div class="col-sm-2"></div>

                    <div class="col-sm-8">
                        <div class="white-box p-l-20 p-r-20">
                            <h3 class="box-title m-b-0"></h3>
                          
                            <div class="row">
                                <div class="col-md-12">
                                     <form action="{{route('ruller.update', $ruller->id)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">

                 <input type="hidden" name="_method" value="PATCH">

                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       
                                        </div> 
                                        <div class="form-group">
                                        <label class="col-sm-12">Dərs Saatı</label>
                                            <div class="col-md-9">
                                                <input name="time"  type="time" class="form-control form-control-line" value="{{$ruller->time}}" placeholder="Dərs Saatı"> </div>
                                        </div> 
                                         <div class="form-group">
                                         <label class="col-sm-12">Dərs Günü</label>
                                            <div class="col-md-9">

                                                <input name="date"  type="date" class="form-control form-control-line" value="{{$ruller->date}}" placeholder="Hansı Günlər(1,2,3,4,5)"> </div>
                                        </div> 
                                    

                                         <div class="form-group">
                                            <label class="col-sm-12">Müəllim</label>
                                            <div class="col-sm-9">
                                                <select name="teacher" class="form-control form-control-line">

                                                    @foreach($users as $user )
                                                    @if($user->type==7)
                                                    <option>{{$user->name}}</option>
                                                    @endif
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>


                                         <div class="form-group">
                                            <label class="col-sm-12">Qrup</label>
                                            <div class="col-sm-9">
                                                <select name="group_id" class="form-control form-control-line">

                                                    @foreach($group as $grup )
                                                    <option value="{{$grup->id}}" >{{$grup->number}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-12">Fenn</label>
                                            <div class="col-sm-9">
                                                <select name="subject_id" class="form-control form-control-line">
                                                <option value="{{$ruller->lesson->id}}">{{$ruller->lesson->name}}</option>
                                                    @foreach($lesson as $lesso)
                                                    <option value="{{$lesso->id}}">{{$lesso->name}}</option>
                                                    @endforeach

                                                    

                                                </select>
                                            </div>
                                        </div>
                                        
                                      
                                        <div class="form-group">
                                            
                                            <div class="col-sm-4"></div>
                                            
                                            <div class="col-sm-4">
                                            <br>

                     <input style="margin-top: 1vw;" type="submit" value="Göndər" class="btn btn-block btn-outline btn-info"  > 
                                                 </div>
                                        </div>

                                    </form>
</div>
<br>
@if ($message = Session::get('success'))
                  <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                          <strong>{{ $message }}</strong>
                  </div>
                @endif
              
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                   

<!-- ent layout -->

@endsection

