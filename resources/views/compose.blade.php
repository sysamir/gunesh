@extends('layouts.app')

@section('content')

<!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- row -->
                <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-2 col-md-3  col-sm-4 col-xs-12 inbox-panel">
                                  <div> <a href="/compose" class="btn btn-custom btn-block waves-effect waves-light">Yeni Mesaj</a>
                                       <div class="list-group mail-list m-t-20">
                                       <a href="/inbox" class="list-group-item active">Daxil olan
                                       <span class="label label-rouded label-success pull-right">
                                       {{$in->count()}}
                                       </span></a>
                                       <a href="/sent-mail" class="list-group-item ">Göndərilən<span class="label label-rouded label-warning pull-right">
                                       {{$out->count()}}
                                       </span></a>
                                       @if(auth()->user()->type == 0)<a href="/all_messages" class="list-group-item ">Bütün mesajlar</a>@endif
                                       </div>
                                       <hr class="m-t-5">
                                   </div>
                                </div>
                                <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 mail_listing">
                                <form action="/compose/insert" method="POST" enctype="multipart/form-data" class="">
                                {{csrf_field()}}
                                    <h3 class="box-title">Yeni Mesaj Yaz</h3>

                                    <h5 class="m-t-20">Göndəriləcək istifadəçilər</h5>
                                    <select name="to_users[]" class="select2 m-b-10 select2-multiple" multiple="multiple" data-placeholder="Choose">
                                        @if(count($students)>0)
                                        <optgroup label="Tələbələr">
                                          @foreach($students as $user)
                                            <option value="{{$user->id}}">{{$user->name.' '.$user->surname}}</option>
                                          @endforeach
                                        </optgroup>
                                        @endif

                                        @if(count($students)>0)
                                        <optgroup label="Müəllimlər">
                                          @foreach($teachers as $user)
                                            <option value="{{$user->id}}">{{$user->name.' '.$user->surname}}</option>
                                          @endforeach
                                        </optgroup>
                                        @endif


                                        <optgroup label="Müdiriyyət">
                                          @if(count($branchadmin)>0)
                                            @foreach($branchadmin as $user)
                                              <option value="{{$user->id}}">{{$user->name.' '.$user->surname}} (F.M)</option>
                                            @endforeach
                                          @endif

                                          @if(count($receptions)>0)
                                            @foreach($receptions as $user)
                                              <option value="{{$user->id}}">{{$user->name.' '.$user->surname}} (Reception)</option>
                                            @endforeach
                                          @endif

                                          @if(count($superadmin)>0)
                                            @foreach($superadmin as $user)
                                              <option value="{{$user->id}}">Rəhbər #{{$user->id}}</option>
                                            @endforeach
                                          @endif
                                        </optgroup>


                                        @if(count($parents)>0)
                                        <optgroup label="Valideyinlər">
                                          @foreach($parents as $user)
                                            <option value="{{$user->id}}">{{$user->name.' '.$user->surname}}</option>
                                          @endforeach
                                        </optgroup>
                                        @endif
                                    </select>

                                    <script>
                                      jQuery(document).ready(function () {
                                          // For select 2
                                          $(".select2").select2();
                                      });
                                    </script>


                                    <div class="form-group">

                                          <div class="form-group">
                                        <textarea name="message" class="textarea_editor form-control" rows="15" placeholder="Mesajı daxil edin ..."></textarea>
                                    </div>
                                    <h4><i class="ti-link"></i> File Seç</h4>
                                    <div class="dropzone">
                                        <div class="fallback">
                                            <input name="attachment" type="file" /> </div>
                                    </div>
                                    </form>
                                    <hr>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Göndər</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" theme="default" class="default-theme working">1</a></li>
                                <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                                <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                                <li><b>With Dark sidebar</b></li>
                                <br/>
                                <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
                            </ul>
                            <ul class="m-t-20 all-demos">
                                <li><b>Choose other demos</b></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="gunesh/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="gunesh/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="gunesh/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="gunesh/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="gunesh/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="gunesh/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="gunesh/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="gunesh/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
@stop
