@extends('layouts.app') @section('content')
<!-- ============================================================== -->
<!-- End Left Sidebar -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
{{--
<div class="container">
    <div class="row white-box p-l-20 p-r-20"> --}}
        <div style="text-align: center;" class="col-md-12">
            <i style="font-size: 18em;" class="fa fa-address-card-o fa-5x" aria-hidden="true"></i>

            <form action="/invite/connect" method="POST" class="form-horizontal form-material">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-md-12">Zəhmət Olmasa Kart-İd daxil Et</label>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <input class="form-control" name="kart_id" type="number" placeholder="Övladınızın kart nömrəsini daxil edin">
                        <br>
                        <input class="fcbtn btn btn-primary btn-outline btn-1e" type="submit" value="Göndər">
                    </div>
                </div>
            </form>

        </div>
        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- .row -->
                <div class="row el-element-overlay m-b-40">
                  @if(count($invites->where('status', '1'))>0)
                    <div class="col-md-12">
                        <h4>Təsdiq Olunmuş Tələbələr<br/></h4>
                        <hr>
                    </div>
                    <!-- /.usercard -->
                    @foreach($invites->where('status', '1') as $invite)

                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <div class="el-card-item">
                                <div class="el-card-avatar el-overlay-1">

                                  @if($invite->students->img==null)
                                      <img src="/uploads/default.png" />
                                  @else
                                      <img src="/uploads/{{$invite->students->img}}" />
                                  @endif

                                    <div class="el-overlay">
                                        <ul class="el-info">
                                            <li>
                                                @if($invite->students->img==null)
                                                <a class="btn default btn-outline image-popup-vertical-fit" href="/uploads/default.png">
                                            @else
                                            <a class="btn default btn-outline image-popup-vertical-fit" href="/uploads/{{$invite->students->img}}">
                                              @endif
                                            <i class="icon-magnifier">

                                            </i>
                                            </a></li>
                                            <li><a class="btn default btn-outline" href="/student/{{$invite->students->id}}"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="el-card-content">
                                    <h3 class="box-title">{{$invite->students->name}}</h3>
                                    <small>{{$invite->students->surname}}</small>
                                    <br/> </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif

                    @if(count($invites->where('status', '0'))>0)
                    <div class="col-md-12">
                        <h4>Gözləmədə Olan Tələbələr<br/></h4>
                        <hr>
                    </div>
                    @foreach($invites->where('status', '0') as $invite)

                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <div class="el-card-item">
                                <div class="el-card-avatar el-overlay-1"> <img src="/uploads/default.png" />
                                    <div class="el-overlay">
                                        <ul class="el-info">

                                        </ul>
                                    </div>
                                </div>
                                <div class="el-card-content">
                                    <h3 class="box-title">#{{$invite->students_invited->kart_id}}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    <!-- /.usercard-->

                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            @stop
