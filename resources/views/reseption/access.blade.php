@extends('layouts.app')


@section('content')
<div class="container">

    <div id="page-wrapper">
            <div class="container-fluid">
              
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">Bütün İstifadəçilər</h3>
                           
                            <div class="table-responsive">
                                <table class="table color-table primary-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Valideyinin Adı</th>
                                            <th>Tələbənin Adı</th>
                                            <th>Qəbul Et</th>
                                            <th>Sil</th>

                                            <th></th>
                                            <th></th>



                                        </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                    @foreach($invites as $invite)
                                    @if($invite->status==2)
                                    <td></td>
                                        <td>{{$invite->parents->name}}</td>
                                        <td>{{$invite->students->name}}</td>
                                        <form action="/reseption/{{$invite->id}}" method="POST">
                                            {{ csrf_field() }}

                                        <td><button class="btn btn-success" value="1" name="status" type="submit">Qəbul Et</button></td>
                                        <td><button class="btn btn-danger" value="3" name="status" type="submit">Sil</button></td>
                                        </form>
                                        @endif
                                        
                                        @endforeach 
                                    </tr>

                                
                                 	
                                    </tbody>
                                </table>
                                @if ($message = Session::get('success'))
                  <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                          <strong>{{ $message }}</strong>
                  </div>
                @endif
                @if ($message = Session::get('delete'))
                  <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                          <strong>{{ $message }}</strong>
                  </div>
                @endif
                            </div>
                        </div>
                    </div>
                    
                   
                </div>


@stop