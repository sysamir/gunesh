@extends('layouts.app')

@section('content')
<div class="col-sm-12">
                    <div class="col-sm-5">
                        <div class="white-box">
                          @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
            </div>
          @endif
                            <h3 class="box-title m-b-0">Sertifikat Formu</h3>
                            <p class="text-muted m-b-30 font-13">  </p>
                            <form action="{{url('/sertified',$id)}}" enctype="multipart/form-data"  method="POST" class="form-horizontal">
                             <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label class="col-md-12">Başlıq</label>
                                    <div class="col-md-12">
                                        <input type="text"  name="title" class="form-control" placeholder="Başlıq"> </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-12">Fayl Yüklə</label>
                                    <div class="col-sm-12">
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Faylı Seç</span> <span class="fileinput-exists">Change</span>
                                            <input type="file" name="file"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Saxla</button>
                                <br>

                            </form>
                        </div>
                    </div>
                    <div class="col-sm-7">

                      <div class="white-box">
                          <h3 class="box-title">Sertifikatlar</h3>
                          <div class="table-responsive">
                              <table class="table">
                                  <thead>
                                      <tr>
                                          <th>#</th>
                                          <th>Sertifikat</th>
                                          <th>Funksiya</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($serfieds as $sertified)
                                      <tr>
                                          <td>{{$sertified->id}}</td>
                                          <td>{{$sertified->title}}</td>

                                          <td><a target="_blank" href="{{url('/sertified/'.$sertified->file)}}"><span class="label label-success">Fayla baxış</span></a></td>
                                          <td><a href="{{url('/sertified/delete/'.$sertified->id)}}"><span class="label label-danger">Sil</span></a></td>
                                      </tr>
                                      @endforeach
                                  </tbody>
                              </table>
                          </div>
                      </div>


                    </div>
                </div>

@stop
