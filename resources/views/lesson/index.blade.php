@extends('layouts.app')

@section('content')
<!-- start -->
<div class="container">
  <div id="page-wrapper">
  <!-- div class="container-fluid">
      <div class="row bg-title">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h4 class="page-title"></h4> </div>
          <!-- /.col-lg-12 -->
     <!--  </div>  -->
      <!-- /row -->
      <div class="row">
          <div class="col-sm-12">
              <div class="white-box">
                  <h3 class="box-title">Sistemdə Olan Bütün Dərslər </h3>

                  <div class="table-responsive">
                      <table class="table color-table primary-table">
                          <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Dərs Adı</th>
                                 <th><i class="fa fa-edit" aria-hidden="true"></i></th>
                                 <th><i class="fa fa-trash-o" aria-hidden="true"></i></th>

                              </tr>
                          </thead>
                          <tbody>

                          @foreach ($lesson as $lesso)
                              <tr>
                                  <td>1</td>
                                  <td>{{$lesso->name}}</td>

                                  <td><a class="btn btn-warning" href="/lesson/{{$lesso->id}}/edit">Redaktə Et</a></td>
                                <td>
                                   <form method="POST" action="{{ route('lesson.destroy', $lesso->id) }}">
                                   {{csrf_field()}}
    <input type="hidden" name="_method" value="DELETE" />
    <button type="submit" class="btn btn-danger">Sil</button>

</form>
</td>   

                              </tr>
                              @endforeach

                          </tbody>

                      </table>
                      @if ($message = Session::get('success'))
                  <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                          <strong>{{ $message }}</strong>
                  </div>
                @endif
                @if ($message = Session::get('delete'))
                  <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                          <strong>{{ $message }}</strong>
                  </div>
                @endif
                  </div>
              </div>
          </div>
      </div>
<!-- endlay out -->
@stop
