
@extends('layouts.app')


@section('content')
<div class="container">

  <div class="row">
                    <div  style="text-align: center;" class="col-sm-12">
                        <div  class="white-box p-l-20 p-r-20">
                        <i  class="fa fa-edit fa-5x"></i>
                            <h3 class="box-title m-b-0">Dərs Adını Dəyiş</h3>

                            <div class="row">
                                <div class="col-md-12">
              <form action="{{route('lesson.update', $lesson->id)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">

                 <input type="hidden" name="_method" value="PATCH">
                 {{ csrf_field() }}

                                        <div class="form-group">
                                             <div class="col-sm-4"></div>

                                            <div  class="col-md-4">
                                                <input value="{{$lesson->name}}" name="name" type="text" class="form-control form-control-line">
                                                 </div>
                                        </div>

                                        <div class="form-group">
                                             <div class="col-sm-4"></div>
                                            <div class="col-sm-4">
                                                <input type="submit" value="Dəyiş" class="btn btn-block btn-success">
                                                 </div>
                                        </div>

                                    </form>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
