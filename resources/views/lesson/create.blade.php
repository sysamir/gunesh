 
@extends('layouts.app')


@section('content')
<div class="container">

  <div class="row">
                    <div  style="text-align: center;" class="col-sm-12">
                        <div  class="white-box p-l-20 p-r-20">
                        <i  class="fa fa-book fa-5x"></i> 
                            <h3 class="box-title m-b-0">Dərs Adını Daxil Et</h3>
                          
                            <div class="row">
                                <div class="col-md-12">
 <form action="/lesson" enctype="multipart/form-data" method="POST" class="form-horizontal">
                                  
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          
                                        <div class="form-group">
                                             <div class="col-sm-4"></div>

                                            <div  class="col-md-4">
                                                <input placeholder="Dərs Adı Əlavə Et" name="name" type="text" class="form-control form-control-line">
                                                 </div>
                                        </div> 
                                        
                                        <div class="form-group">
                                             <div class="col-sm-4"></div>
                                            <div class="col-sm-4">
                                                <input type="submit" value="Göndər" class="btn btn-block btn-success" placeholder="Helping text"> 
                                                 </div>
                                        </div>
@if ($message = Session::get('success'))
                  <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                          <strong>{{ $message }}</strong>
                  </div>
                @endif
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<!-- ent layout -->


@endsection

