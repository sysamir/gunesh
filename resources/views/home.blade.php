@extends('layouts.app')

@section('content')
<div class="container">
@if(Auth::user()->type==0)
  <div class="row">
                            <div class="col-lg-3 col-sm-6 col-xs-12">
                                <div class="white-box">
                                    <h3 class="box-title">Tələbələr</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-people text-info"></i></li>
                                        <li class="text-right"><span class="counter">{{$students->count()}}</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-12">
                                <div class="white-box">
                                    <h3 class="box-title">Valideyinlər</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-people text-info"></i></li>
                                        <li class="text-right"><span class="counter">{{$parents->count()}}</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-12">
                                <div class="white-box">
                                    <h3 class="box-title">Reseptionlar</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-people text-info"></i></li>
                                        <li class="text-right"><span class="">{{$reseption->count()}}</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-12">
                                <div class="white-box">
                                    <h3 class="box-title">Filial Rəhbərləri</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-people text-danger"></i></li>
                                        <li class="text-right"><span class="">{{$branchadmin->count()}}</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-12">
                                <div class="white-box">
                                    <h3 class="box-title">Filiallar</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-folder-alt text-info"></i></li>
                                        <li class="text-right"><span class="">{{$branch->count()}}</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-12">
                                <div class="white-box">
                                    <h3 class="box-title">Quruplar</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-folder-alt text-danger"></i></li>
                                        <li class="text-right"><span class="">{{$group->count()}}</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-12">
                                <div class="white-box">
                                    <h3 class="box-title">CPU</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="ti-save text-success"></i></li>
                                        <li class="text-right"><span style="font-size: 37px;" class="">{{$load[0]}}%</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-12">
                                <div class="white-box">
                                    <h3 class="box-title">Ram Yaddaşı</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="ti-server text-success"></i></li>
                                        <li class="text-right"><span style="font-size: 37px;" class="">{{$ram}}</span></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        @endif
                        </div>


@endsection
