@extends('layouts.app') @section('content')
<!-- start -->

<div class="container">

    <div id="page-wrapper">

        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title">Sistemdə Olan Bütün Quruplar </h3>

                    <div class="table-responsive">
                        <table class="table color-table primary-table">
                            <thead>
                                <tr>

                                    <th>#</th>
                                    <th>Qurup Nömrəsi</th>
                                    <th>Qurupun Filialı</th>
                                    <th width="6%"><i class="fa fa-edit" aria-hidden="true"></i></th>
                                    <th width="6%"><i class="fa fa-trash-o" aria-hidden="true"></i></th>

                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($groups as $group)
                                <tr>

                                    <td>1</td>
                                    <td>{{$group->number}}</td>
                                    <td>{{$group->branch->name}}</td>

                                    <td><a class="btn btn-warning" href="/group/{{$group->id}}/edit">Redaktə Et</a></td>
                                    <td>
                                        <form method="POST" action="{{ route('group.destroy', $group->id) }}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <button type="submit" class="btn btn-danger">Sil</button>

                                        </form>
                                    </td>

                                </tr>
                                @endforeach

                            </tbody>

                        </table>
                        @if ($message = Session::get('delete'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>

        <!-- endlay out -->
        @stop
