@extends('layouts.app') @section('content')

<!-- ============================================================== -->
<!-- End Left Sidebar -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Profil Səhifəsi </h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->

        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="white-box">
                    <div class="user-bg">
                        @if( Auth::user()->img!=null)
                        <img width="100%" alt="user" src="/uploads/{{ Auth::user()->img }}"> @else
                        <img width="100%" alt="user" src="/uploads/default.png"> @endif
                        <div class="overlay-box">
                            <div class="user-content">
                                <a href="javascript:void(0)">
                                       @if( Auth::user()->img!=null)

                                        <img src="/uploads/{{ Auth::user()->img }}" class="thumb-lg img-circle" alt="img">
                                        @else
                                        <img src="/uploads/default.png" class="thumb-lg img-circle" alt="img">

                                        @endif

                                        </a>

                                <h4 class="text-white">{{ Auth::user()->username }}</h4>
                                <h5 class="text-white">{{ Auth::user()->email }}</h5> </div>

                        </div>
                    </div>
                    <div class="user-btm-box">
                        @if($p)
                        <p class="text-danger">Valdeyinizi Təsdiq Etdinizsə Qəbul Ed Düyməsini Sıxın</p>
                        <br>
                        <p>Ad Soyad</p>- {{$p->parents->name.' '.$p->parents->surname}}
                        <form action="{{url('/students')}}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="parent" value="{{$p->parents->id}}">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <button class="btn btn-block btn-outline btn-info" name="status" value="1" type="submit">Qəbul et</button>

                                <button class="btn btn-block btn-outline btn-info" name="status" value="2" type="submit">Imtina et</button>

                            </div>
                            @endif

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-xs-12">
                <div class="white-box">
                    <ul class="nav nav-tabs tabs customtab">
                        <li class="active tab">
                            <a href="#home" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Uğurlar</span> </a>
                        </li>
                        <li class="tab">
                            <a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Profil</span> </a>
                        </li>
                        <li class="tab">
                            <a href="#messages" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Bildirişlər</span> </a>
                        </li>
                        <li class="tab">
                            <a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Tənzimləmələr</span> </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="home">
                            <div class="steamline">
                                <!-- <div class="sl-item">
                                            <div class="sl-left"> <img src="/gunesh/images/users/genu.jpg" alt="user" class="img-circle" /> </div>
                                            <div class="sl-right">
                                                <div class="m-l-40"><a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                    <p>assign a new task <a href="#"> Design weblayout</a></p>
                                                    <div class="m-t-20 row"><img src="/gunesh/images/img1.jpg" alt="user" class="col-md-3 col-xs-12" /> <img src="/gunesh/images/img2.jpg" alt="user" class="col-md-3 col-xs-12" /> <img src="/gunesh/images/img3.jpg" alt="user" class="col-md-3 col-xs-12" /></div>
                                                </div>
                                            </div>
                                        </div> -->

                                @foreach(Auth::user()->sertified as $sert )
                                <div class="sl-item">
                                    <div class="sl-left"> <img src="/uploads/default.png" alt="user" class="img-circle" /> </div>
                                    <div class="sl-right">
                                        <div class="m-l-40">
                                            <a href="#" class="text-info">{{$sert->title}}</a>
                                            <span class="sl-date">{{$sert->updated_at->format('Y-m-d')}}</span>
                                            <p><a href="/sertified/{{$sert->file}}">{{$sert->title}}</a></p>

                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                        <div class="tab-pane" id="profile">
                            <div class="row">
                                <div class="col-md-3 col-xs-6 b-r"> <strong> Ad Soyad</strong>
                                    <br>
                                    <p class="text-muted">{{ Auth::user()->name }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Mobil No</strong>
                                    <br>
                                    <p class="text-muted">{{ Auth::user()->mobile }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                    <br>
                                    <p class="text-muted">{{ Auth::user()->email }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6"> <strong>Doğum Tarixi</strong>
                                    <br>
                                    <p class="text-muted">{{ Auth::user()->date }}</p>
                                </div>
                            </div>
                            <hr>
                            <p class="m-t-30">{{ Auth::user()->about }}</p>

                            <h4 class="font-bold m-t-30">İnkişaf Mərhələsi </h4>
                            <hr>

                        </div>
                        <div class="tab-pane" id="messages">
                            <div class="steamline">
                                <!-- <div class="sl-item">
                                            <div class="sl-left"> <img src="/gunesh/images/users/genu.jpg" alt="user" class="img-circle" /> </div>
                                            <div class="sl-right">
                                                <div class="m-l-40"> <a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                    <div class="m-t-20 row">
                                                        <div class="col-md-2 col-xs-12"><img src="/gunesh/images/img1.jpg" alt="user" class="img-responsive" /></div>
                                                        <div class="col-md-9 col-xs-12">
                                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa</p> <a href="#" class="btn btn-success"> Design weblayout</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                <hr>
                                <div class="sl-item">

                                    <div class="sl-right">
                                        <div class="m-l-40">
                                            <div class="m-t-20 row">
                                                <div class="col-md-2 col-xs-12"></div>
                                                <div class="col-md-9 col-xs-12">
                                                    <p> Bildiriş Yoxdur </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane" id="settings">
                            <form action="/profile/update" method="POST" enctype="multipart/form-data" class="form-horizontal form-material">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label class="col-md-12">Ad</label>
                                    <div class="col-md-12">
                                        <input name="name" value="{{ Auth::user()->name }}" type="text" placeholder="" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Soyad</label>
                                    <div class="col-md-12">
                                        <input name="surname" value="{{ Auth::user()->surname }}" type="text" placeholder="" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <input name="email" value="{{ Auth::user()->email }}" type="email" placeholder="" class="form-control form-control-line" name="example-email" id="example-email"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Şifrə</label>
                                    <div class="col-md-12">
                                        <input name="password" type="password" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Şifrə Tekrar</label>
                                    <div class="col-md-12">
                                        <input name="password_confirmation" type="password" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Mobil No</label>
                                    <div class="col-md-12">
                                        <input name="mobile" value="{{ Auth::user()->mobile }}" type="text" placeholder="" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Dogum Tarix</label>
                                    <div class="col-md-12">
                                        <input name="date" value="{{ Auth::user()->date }}" type="date" placeholder="" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Foto</label>
                                    <div class="col-md-12">
                                        <input name="img" type="file" placeholder="" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Haqqinda</label>
                                    <div class="col-md-12">
                                        <textarea name="about" rows="5" class="form-control form-control-line">
                                            {{ Auth::user()->about }}
                                        </textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success"> Yenilə</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        @endsection
