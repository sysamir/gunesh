@extends('layouts.app')


@section('content')


        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Profil Səhifəsi </h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">


                    </div>
                </div>
                <!-- /.row -->
                <!-- .row -->

                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg">
                            @if( $user->img!=null)
                            <img width="100%" alt="user" src="/uploads/{{ $user->img }}">
                            @else
                            <img width="100%" alt="user" src="/uploads/default.png">
                            @endif
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)">
                                       @if( $user->img!=null)

                                        <img src="/uploads/{{ $user->img }}" class="thumb-lg img-circle" alt="img">
                                        @else
                                        <img src="/uploads/default.png" class="thumb-lg img-circle" alt="img">

                                        @endif

                                        </a>

                                        <h4 class="text-white">{{ $user->name.' '.$user->surname }}</h4>
                                        <h5 class="text-white">{{ $user->email }}</h5> </div>


                                </div>
                            </div>
                            <div class="user-btm-box">


                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                            <ul class="nav nav-tabs tabs customtab">
                                <li class="active tab">
                                    <a href="#home" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Uğurlar</span> </a>
                                </li>
                                <li class="tab">
                                    <a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Profil</span> </a>
                                </li>
                                <li class="tab">
                                    <a href="#messages" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Bildirişlər</span> </a>
                                </li>
                                
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="home">
                                    <div class="steamline">
                                      @foreach($user->sertified as $sert )
                                         <div class="sl-item">
                                          <div class="sl-left"> <img src="/uploads/default.png" alt="user" class="img-circle" /> </div>
                                          <div class="sl-right">
                                              <div class="m-l-40">
                                              <a href="#" class="text-info">{{$sert->title}}</a>
                                              <span class="sl-date">({{$sert->updated_at->format('Y-m-d')}})</span>
                                                  <p><a href="/sertified/{{$sert->file}}">Fayla baxış</a></p>

                                              </div>
                                          </div>
                                      </div>
                                      @endforeach



                                    </div>
                                </div>
                                <div class="tab-pane" id="profile">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-6 b-r"> <strong> Ad Soyad</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->name }}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Mobil No</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->mobile }}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->email }}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6"> <strong>Doğum Tarixi</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->date }}</p>
                                        </div>
                                    </div>
                                    <hr>
                                    <p class="m-t-30">{{ $user->about }}</p>

                                    <h4 class="font-bold m-t-30">İnkişaf Mərhələsi </h4>
                                    <hr>

                                </div>
                                <div class="tab-pane" id="messages">
                                    <div class="steamline">
                                        <!-- <div class="sl-item">
                                            <div class="sl-left"> <img src="/gunesh/images/users/genu.jpg" alt="user" class="img-circle" /> </div>
                                            <div class="sl-right">
                                                <div class="m-l-40"> <a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                    <div class="m-t-20 row">
                                                        <div class="col-md-2 col-xs-12"><img src="/gunesh/images/img1.jpg" alt="user" class="img-responsive" /></div>
                                                        <div class="col-md-9 col-xs-12">
                                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa</p> <a href="#" class="btn btn-success"> Design weblayout</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->


                                        <hr>
                                        <div class="sl-item">

                                            <div class="sl-right">
                                                <div class="m-l-40">
                                                    <div class="m-t-20 row">
                                                        <div class="col-md-2 col-xs-12"></div>
                                                        <div class="col-md-9 col-xs-12">
                                                            <p> Bildiriş Yoxdur </p> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

@endsection
