@extends('layouts.app') @section('content')

<div class="container">

  <div id="page-wrapper">
      <div class="container-fluid">

          <!-- /row -->
          <div class="row">
            <div class="col-lg-12">
                      <div class="white-box">
                          <h3 class="box-title m-b-0">Muellim cadvəl üzrə davamiyyəti </h3>
                          <div class="table-responsive">
                              <table class="table">
                                  <thead>
                                      <tr>
                                          <th></th>
                                          <th>Tələbə</th>
                                          <th>İştirak</th>
                                          <th>Tarix</th>

                                      </tr>
                                  </thead>
                                  <tbody>
                                    <form id="ListForm" class="" action="{{url('/attendance/check')}}" method="post" enctype="multipart/form-data">
                                      {{csrf_field()}}
                                      <input type="hidden" name="ruller_id" value="{{$id}}">
                                    @foreach($users as $user)
                                    @if($user->type==7)
                                      <tr>
                                          <td>
                                              <input name="action[]" value="{{$user->id}}" type="hidden">
                                              
                                          </td>

                                          <td>{{$user->name.' '.$user->surname}}</td>
                                          <td>
                                            @if(count($user->attendance->where('ruller_id',$id)->first())>0)
                                              @if($user->attendance->where('ruller_id',$id)->first()->status == 1)
                                                <div class="label label-table label-success">Gəlib</div>
                                              @elseif($user->attendance->where('ruller_id',$id)->first()->status == 2)
                                                <div class="label label-table label-danger">Gəlməyib</div>
                                              @elseif($user->attendance->where('ruller_id',$id)->first()->status == 3)
                                                <div class="label label-table label-warning">Gecikib</div>
                                              @endif
                                            @else
                                            ---
                                            @endif
                                          </td>
                                          <td>
                                            @if(count($user->attendance->where('ruller_id',$id)->first())>0)
                                              {{$user->attendance->where('ruller_id',$id)->first()->updated_at}}
                                            @else
                                            ---
                                            @endif
                                          </td>
                                      </tr>
                                      @endif
                                      @endforeach
                                  </tbody>
                              </table>

                            {{--   <div class="col-sm-3">
                                  <select id="action" name="status" class="form-control">
                                      <option selected disabled> -- Seçin --</option>
                                      <option value="1">İştirak edib</option>
                                      <option value="2">Etməyib</option>
                                      <option value="3">Gecikib</option>
                                  </select>
                              </div> --}}

                              <button name="status" type="submit" value="1" class="btn btn-success">İştirak edib</button>
                              <button name="status" type="submit" value="2" class="btn btn-danger">Etməyib</button>
                              <button name="status" type="submit" value="3" class="btn btn-warning">Gecikib</button>


                            
                            </form>
                          </div>
                      </div>
                  </div>
          </div>
  @stop
