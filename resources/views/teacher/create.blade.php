@extends('layouts.app')


@section('content')
<style type="text/css">
    label{
        right: 1vw;
    }
</style>

<div class="container">

  <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box p-l-20 p-r-20">
                          
                            <div class="row">
                                <div class="col-md-12">
                         <form action="{{url('/teacher')}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                            <label class="col-md-12">Ad</label>

                                                <input  placeholder="Ad" name="name" type="text" class="form-control form-control-line"> </div>

                                                <!-- user -->
                                                <div class="col-md-6">
                                            <label class="col-md-12">Soyad</label>

                                                <input placeholder="Soyad" name="surname" type="text" class="form-control form-control-line"> </div>
                                        </div> 
                                        <div class="form-group">
                                            <div class="col-md-6">
                                            <label class="col-md-12">Email</label>

                                                <input placeholder="e-mail" type="email" id="example-email2" name="email" class="form-control form-control-line"> </div>

                                                <!-- user -->
                                                <div class="col-md-6">
                                            <label class="col-md-12">Şifrə </label>

                                                <input  name="password" type="password" class="form-control form-control-line"> </div>
                                        </div> 
                                         <div class="form-group">
                                            <div class="col-md-6">
                                            <label class="col-md-12">Mobil No</label>

                                                <input placeholder="Mobil No" name="mobile"  type="text" class="form-control form-control-line" > </div> 
                                                <div class="col-md-6"> 
                                                <label  class="col-sm-6">Vəzifə</label>
                                               <div  style="padding-left: 0px;" style="padding-left: 0px;" class="col-sm-12">
                                                <select name="type" class="form-control form-control-line">

                                                     
                                                    <option value="1">Tələbə</option>
                                                    <option value="2">Valideyin</option>
                                                     <option value="7">Muellim</option>

                                                    

                                                    
                                                    
                                                </select>
                                            </div></div>
                                                </div>

                                      
                                        

                                         <div class="form-group">
                                            <div class="col-md-6">
                                                    <label class="col-sm-12">Filial</label>
                                                    <div style="padding-left: 0px;" class="col-md-12">
                                                       <select name="group_id" class="form-control form-control-line">
        @foreach($branch->all() as $branc)
                        <optgroup label="{{$branc->name}}">
                            @foreach($branc->group->all() as $grop)
                                <option value="{{$grop->id}}">{{$grop->number}}</option>
                            @endforeach
                        </optgroup>
                    @endforeach
            </select>
                                                    </div>

                                                    </div>
                                                     <div class="col-md-6">
                                            <label  class="col-sm-12">Doğum Tarixi </label>
                                            <div style="padding-left: 0px;" class="col-sm-12">
                                                <input type="date"  name="date" class="form-control form-control-line" placeholder="Helping text"> 
                                                <span class="help-block">
                                     <small>Məlumatları Yadda Saxlamaq Üçün Göndər  Düyməsini Sıx.</small></span> </div>
                                        </div>
                                        </div>
                                        <div class="form-group">

                                            <div class="col-sm-6">
                                               <label>Şəkil Yüklə</label>

                                          <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control form-control-line" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new"> Foto </span> <span class="fileinput-exists">Dəyiş</span>
                                                    <input type="file" name="img"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Sil</a> </div>
                                            </div>
                                               <div class="col-md-6">
                                               <label>Haqqında</label>
                                                <textarea style="height: 2.5vw;" name="about" class="form-control form-control-line">
                                                    
                                                </textarea>
                                            </div>
                                        </div>

                                    
                                  <div class="form-group">
                                            <div class="col-sm-4"></div>
                                            
                                            <div class="col-sm-4">
                     <input type="submit" value="Göndər" class="btn btn-block btn-outline btn-info"  > 
                                                 </div>
                                        </div>

                                    </form>
                                    @if ($message = Session::get('success'))
                  <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                          <strong>{{ $message }}</strong>
                  </div>
                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<!-- ent layout -->


@endsection