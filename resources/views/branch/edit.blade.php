@extends('layouts.app')


@section('content')

<div class="container">

  <div class="row">
                    <div  style="text-align: center;" class="col-sm-12">
                        <div  class="white-box p-l-20 p-r-20">
                        <i  class="fa fa-users fa-5x"></i> 
                            <h3 class="box-title m-b-0">Filial Əlavə Et</h3>
                          
                            <div class="row">
                                <div class="col-md-12">
                       <form action="{{url('/branch',['id'=>$branch->id])}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {{ method_field('PATCH') }}

                <div class="form-group">
                     <div class="col-sm-4"></div>

                    <div  class="col-md-4">
            <input placeholder="Filial Adını Daxil Et" name="name" type="text" value="{{$branch->name}}" class="form-control form-control-line">
             </div>
                </div> 
                
                <div class="form-group">
                     <div class="col-sm-4"></div>
                    <div class="col-sm-4">
                        <input type="submit" value="Göndər" class="btn btn-block btn-success" placeholder="Helping text"> 
                         </div>
                </div>

                                    </form>
                                       @if ($message = Session::get('success'))
                  <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                          <strong>{{ $message }}</strong>
                  </div>
                @endif
               
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<!-- ent layout -->


@endsection