@extends('layouts.app')

@section('content')
 <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- row -->
                <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-md-12">
                        <div class="white-box">
                            <!-- row -->
                            <div class="row">
                                <div class="col-lg-2 col-md-3  col-sm-12 col-xs-12 inbox-panel">
                                   <div> <a href="/compose" class="btn btn-custom btn-block waves-effect waves-light">Yeni Mesaj</a>
                                        <div class="list-group mail-list m-t-20">
                                        <a href="/inbox" class="list-group-item active">Daxil olan
                                        <span class="label label-rouded label-success pull-right">
                                        {{$in->count()}}
                                        </span></a>
                                        <a href="/sent-mail" class="list-group-item ">Göndərilən<span class="label label-rouded label-warning pull-right">
                                        {{$out->count()}}
                                        </span></a>
                                        @if(auth()->user()->type == 0)<a href="/all_messages" class="list-group-item ">Bütün mesajlar</a>@endif
                                        </div>
                                        <hr class="m-t-5">
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12 mail_listing">
                                    <div class="inbox-center">
                                        <table class="table table-hover">
                                            <thead>
                                                  @foreach($chat as $cha)
                                                <tr>
                                                  <td>
                                                    @if($cha->status==0)
                                                    <span class="fa fa-circle text-black m-r-10">
                                                    @endif
                                                    </span>
                                                  </td>
                                                    <td class="hidden-xs">{{$cha->user->name.' '.$cha->user->surname}}</td>
                                                    <td class="max-texts">
                                                      <a href="/single-message/{{$cha->id}}">
                                                        {{ str_limit($cha->message, 50) }}
                                                        {{$cha->message ? '' : '- Boş Mesaj -'}}
                                                      </a>
                                                    </td>
                                                    <td class="hidden-xs">
                                                    @if($cha->attachment!=null)
                                                    <i class="fa fa-paperclip"></i>
                                                    @endif
                                                    </td>
                                                    <td class="text-right"> {{$cha->created_at}} </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-7 m-t-20"></div>
                                        <div class="col-xs-5 m-t-20">
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-left"></i></button>
                                                <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->


@stop
