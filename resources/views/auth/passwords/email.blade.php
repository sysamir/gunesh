<!DOCTYPE html>  
<html lang="en">

<!-- Mirrored from wrappixel.com/ampleadmin/ampleadmin-html/ampleadmin-material/recoverpw.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Jun 2017 22:48:54 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="/gunesh/plugins/images/favicon.png">
<title>Gunesh</title>
<!-- Bootstrap Core CSS -->
<link href="/gunesh/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="/gunesh/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="/gunesh/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="/gunesh/css/colors/default.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>

<section id="wrapper" class="login-register">
  <div class="login-box">
    <div class="white-box">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                           
          <div class="col-xs-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Yenilə</button>
          </div>
        </div>
                    </form>
            </div>
  </div>
</section>
<!-- jQuery -->
<script src="/gunesh/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/gunesh/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="/gunesh/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="/gunesh/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="/gunesh/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="/gunesh/js/custom.min.js"></script>
<!--Style Switcher -->
<script src="/gunesh/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

<!-- Mirrored from wrappixel.com/ampleadmin/ampleadmin-html/ampleadmin-material/recoverpw.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Jun 2017 22:48:54 GMT -->
</html>
