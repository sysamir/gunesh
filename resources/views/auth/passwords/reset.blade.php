<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from wrappixel.com/ampleadmin/ampleadmin-html/ampleadmin-material/recoverpw.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Jun 2017 22:48:54 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="/gunesh/plugins/images/favicon.png">
<title>Gunesh</title>
<!-- Bootstrap Core CSS -->
<link href="/gunesh/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="/gunesh/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="/gunesh/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="/gunesh/css/colors/default.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Preloader -->

<section id="wrapper" class="login-register">
  <div style="border-radius: 3px;" class="login-box">
    <div style="border-radius: 3px;" class="white-box">

                <div class="panel-body" style="
    padding-bottom: 0px;
    padding-top: 21px;
">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                            <div class="col-md-12">
                                <input id="email" placeholder="E-poçt" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                            <div class="col-md-12">
                                <input id="password" placeholder="Yeni şifrə" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input placeholder="Yeni şifrə (təkrar)" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">
                                    Yenilə
                                </button>
                            </div>
                        </div>
                    </form>
                           </div>
  </div>
</section>
<!-- jQuery -->
<script src="/gunesh/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/gunesh/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="/gunesh/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="/gunesh/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="/gunesh/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="/gunesh/js/custom.min.js"></script>
<!--Style Switcher -->
<script src="/gunesh/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

<!-- Mirrored from wrappixel.com/ampleadmin/ampleadmin-html/ampleadmin-material/recoverpw.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Jun 2017 22:48:54 GMT -->
</html>
