<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
<title>Günəş Edu Online Sistem</title>
<!-- Bootstrap Core CSS -->
<link href="/gunesh/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="/gunesh/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="/gunesh/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="/gunesh/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box login-sidebar">
    <div class="white-box">
      <form class="form-horizontal form-material" id="loginform"  method="POST" action="{{ url('/new_register') }}">
            {{ csrf_field() }}
        <a href="javascript:void(0)" class="text-center db">Günəş Təhsil Mərkəzi | Qeydiyyat</a>

        <div class="form-group  form-group{{ $errors->has('name') ? ' has-error' : '' }}" style="margin-bottom:15px !important">
          <div class="col-xs-12">
            <input id="name" type="text" class="form-control" name="name" value="" placeholder="Ad" required autofocus>

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
          </div>
        </div>


        <div class="form-group  form-group{{ $errors->has('surname') ? ' has-error' : '' }}" style="margin-bottom:15px !important">
          <div class="col-xs-12">
            <input id="surname" type="text" class="form-control" name="surname" value="" placeholder="Soyad" required autofocus>

            @if ($errors->has('username'))
                <span class="help-block">
                    <strong>{{ $errors->first('surname') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="form-group form-group{{ $errors->has('email') ? ' has-error' : '' }}" style="margin-bottom:15px !important">
          <div class="col-xs-12">
            <input id="email" type="email" class="form-control" name="email" value="" placeholder="E-Poçt Ünvanı" required>

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="form-group form-group{{ $errors->has('password') ? ' has-error' : '' }}" style="margin-bottom:15px !important">
          <div class="col-xs-12">
            <input type="password" class="form-control" name="password" required placeholder="Şifrə">

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="form-group form-group{{ $errors->has('password') ? ' has-error' : '' }}" style="margin-bottom:15px !important">
          <div class="col-xs-12">
            <input type="password" class="form-control" name="password_confirmation" required placeholder="Şifrə Təkrarı">

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="form-group form-group{{ $errors->has('mobile') ? ' has-error' : '' }}" style="margin-bottom:15px !important">
          <div class="col-xs-12">
            <input type="number" class="form-control" name="mobile" required placeholder="Telefon nömrəsi">

            @if ($errors->has('mobile'))
                <span class="help-block">
                    <strong>{{ $errors->first('mobile') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="form-group form-group{{ $errors->has('type') ? ' has-error' : '' }}" style="margin-bottom:15px !important">
          <div class="col-xs-12">
            <select class="form-group coursd" name="type" style="margin-bottom: 20px;margin-left: 0;">
              <option value="1">Tələbə</option>
              <option value="2">Valideyn</option>
            </select>






<br><style type="text/css">.inputs {
  display: none;
}

.foo:checked + .inputs {
  display: initial;
}</style>

  <label class="foo"><h4>Günəş Kurslarında Təhsil Alırsınız?</h4></label>
  <input  type="checkbox" class="foo">


        <div class="inputs form-group form-group{{ $errors->has('type') ? ' has-error' : '' }}">
          <div class="col-xs-12" style="padding:0!important;">
            <select name="group_id"  class="form-group coursd" style="margin-bottom: 0px;margin-left: 0;">
        @foreach($branch->all() as $branc)
                        <optgroup label="{{$branc->name}}">
                            @foreach($branc->group->all() as $grop)
                                <option value="{{$grop->id}}">{{$grop->number}}</option>
                            @endforeach
                        </optgroup>
                    @endforeach
            </select>

            {{-- <input type="hidden" name="branch_id" value="5"> --}}

<br>
            @if ($errors->has('type'))
                <span class="help-block">
                    <strong>{{ $errors->first('type') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Qeydiyyatdan Keç</button>
          </div>
        </div>
         @if ($message = Session::get('unsuccess'))
                  <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                          <strong>{{ $message }}</strong>
                  </div>
                @endif

      </form>

    </div>
  </div>
</section>
<!-- jQuery -->
<script src="gunesh/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="gunesh/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="gunesh/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="gunesh/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="gunesh/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="gunesh/js/custom.min.js"></script>
<!--Style Switcher -->
<script src="gunesh/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<!-- js -->
<script src="https://code.jquery.com/jquery-1.11.3.js"></script>
<script>
    $(document).ready(function(){
        $("#boxchecked").click(function (){
            if ($("#boxchecked").prop("checked")){
                $("#hidden").hide();
            }else{
                $("#hidden").show();
            }
        });
    });
</script>
</body>

