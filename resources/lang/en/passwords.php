<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Yeni şifrə 6 simvoldan az ola bilməz!',
    'reset' => 'Şifrəniz yeniləndi!',
    'sent' => 'Şifrənin bərpa keçidi daxil etdiyiniz e-poçt adresinə göndərildi!',
    'token' => 'İstifadə etdiyiniz keçid səhvdir',
    'user' => "Bu e-poçt ilə əlaqəli istifadəçi tapılmadı",

];
