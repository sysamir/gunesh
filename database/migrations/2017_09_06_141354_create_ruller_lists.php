<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRullerLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ruller_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->time('day1')->nullable();
            $table->time('day2')->nullable();
            $table->time('day3')->nullable();
            $table->time('day4')->nullable();
            $table->time('day5')->nullable();
            $table->time('day6')->nullable();
            $table->time('day7')->nullable();
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('group')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ruller_lists');
    }
}
