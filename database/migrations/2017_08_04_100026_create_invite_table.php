<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInviteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invite', function (Blueprint $table) {
            $table->increments('id');

         $table->integer('parent_id')->unsigned();
         $table->foreign('parent_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
         $table->integer('student_id')->unsigned();
         $table->foreign('student_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
         $table->integer('status')->default('0');

         $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invite');
    }
}
