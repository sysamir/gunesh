<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create('chat', function (Blueprint $table) {
            $table->increments('id');

         $table->integer('from')->unsigned();
            $table->foreign('from')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('to')->unsigned();
            $table->foreign('to')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
             
         $table->text('message')->nullable();
         $table->string('attachment')->nullable();
            
         $table->integer('status');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chat');
    }
}
