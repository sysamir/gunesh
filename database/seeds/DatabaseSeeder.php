<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'admin',
            'surname'=>'test',
            'email'=>'admin@demo.com',
            'mobile'=>'123456',
            'password'=>bcrypt('123456'),
            'type'=>'0',

        ]);

        DB::table('users')->insert([
            'name'=>'smm',
            'surname'=>'smm',
            'email'=>'smm@demo.com',
            'mobile'=>'123456',
            'password'=>bcrypt('123456'),
            'type'=>'5',

        ]);

    }
}
