<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/new_register', 'UserController@new_register');

// Route::post('/registry', 'HomeController@register');

Auth::routes();
Route::group(['middleware' => ['auth']], function () {

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@profile');

Route::get('teachers/{id}', 'HomeController@index_attendance');

Route::resource('branch', 'BranchController');
Route::resource('branchadmin', 'BranchadminController');
Route::resource('group', 'GroupController');
Route::resource('ruller', 'RullerController');
Route::get('ruller/view', 'RullerController@view');

Route::resource('parents', 'ParentsController');
Route::resource('students', 'StudentsController');

Route::resource('lesson', 'LessonController');


Route::get('profile', 'UserController@profile');
Route::post('profile/update', 'UserController@profile_update');
Route::get('sertified/{id}', 'SertifiedController@create');
Route::post('sertified/{id}', 'SertifiedController@store');
Route::get('sertified/delete/{id}', 'SertifiedController@destroy');

Route::get('/all_messages', 'ChatController@all_messages');




Route::get('/student/{id}', 'InviteController@check');

// Route::resource('reseption', 'ReseptionController');



Route::get('reseption/access', 'ReseptionController@access');
Route::post('reseption/{id}', 'ReseptionController@update');







// InviteController
Route::post('/invite/connect', 'InviteController@invite');

Route::resource('users', 'UserController');
Route::post('/user/status/{id}', 'HomeController@status');



Route::get('attendance/{id}', 'AttendanceController@index');
Route::post('/attendance/check', 'AttendanceController@check');


// ChatController

Route::get('/compose', 'ChatController@compose');
Route::get('/inbox', 'ChatController@inbox');
Route::get('/sent-mail', 'ChatController@sent_mail');

Route::get('/single-message/{id}', 'ChatController@messages_show');


Route::post('/compose/insert', 'ChatController@insert');


});
