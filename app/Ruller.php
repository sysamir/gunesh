<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruller extends Model
{
    protected $table="ruller";

    protected $fillable = [
       'group_id',
       'date',
       'time',
    ];

    function lesson()
    {
    	return $this->belongsTo('App\Lesson','subject_id');
    }
}
