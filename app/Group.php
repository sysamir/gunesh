<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
 protected $table="group";

 protected $fillable = [
        'number', 'branch_id',
    ];

  public function branch()
    {
        return $this->belongsTo('App\Branch');
    }



    }
