<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $table="attendance";

    protected $fillable = [
        'user_id', 'ruller_id', 'group_id','status','price',
    ];
}
