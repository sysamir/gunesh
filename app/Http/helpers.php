<?php

/*
|--------------------------------------------------------------------------
| Detect Active Route
|--------------------------------------------------------------------------
|
| Compare given route with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
function isActiveRoute($route, $output = "active")
{
    if (Route::currentRouteName() == $route) return $output;
}

function isActiveSlug($active, $route, $output = "active")
{
  if ($active == $route) {
    return $output;
  }
}
/*
|--------------------------------------------------------------------------
| Detect Active Routes
|--------------------------------------------------------------------------
|
| Compare given routes with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
function areActiveRoutes(Array $routes, $output = "active")
{
    foreach ($routes as $route)
    {
        if (Route::currentRouteName() == $route) return $output;
    }

}



function folderSize ($dir)
{
  $file_size = 0;

  foreach( File::allFiles($dir) as $file)
  {
      $file_size += $file->getSize();
  }


  return formatBytes($file_size);
}

function formatBytes($size, $precision = 2)
    {
        if ($size > 0) {
            $size = (int) $size;
            $base = log($size) / log(1024);
            $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');

            return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
        } else {
            return $size;
        }
    }

function convert($size)
{
    $unit=array('B','KB','MB','GB','TB','PB');
    return round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}
