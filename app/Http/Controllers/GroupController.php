<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\Branch;


class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        $groups = Group::All();

        return view('group/index',compact('groups')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branches = Branch::All();

        return view('group.create',compact('branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    
     if ($request->number=="") {

       return back()->with('unsuccess', 'Uğursuz! Qurup Nomresini Daxil Edin.');

        
     }else{
        $group = new Group;
        $group->number=$request->number;
        $group->branch_id=$request->branch_id;

        $group->save();

        return back()->with('success', 'Uğurlu! Qurup Əlavə Edildi.');
        
     }

 }
         
        
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $groups = Group::All();

        return view('group/index',compact('groups')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::find($id);
        $branches = Branch::All();

        return view('group/edit',compact('group','branches')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $groupes = Group::find($id);

        $groupes->number=$request->number;
        $groupes->branch_id=$request->branch_id;

        $groupes->save();

        return back()->with('success', 'Uğurlu! Qurup Redaktə Edildi.'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = Group::find($id);

        $group->delete();
        return back()->with('delete', 'Uğurlu! Qurup Silindi.'); 
    }
}
