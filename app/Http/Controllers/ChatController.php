<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Chat;

class ChatController extends Controller
{
    public function compose() {
      $group_id =	auth()->user()->group_id;
      $branch_id =	auth()->user()->branch_id;
      $userid = auth()->user()->id;
      $in = Chat::where('to',$userid)->where('status','0')->get();
      $out = Chat::where('from',$userid)->get();
      if (auth()->user()->type == 0) {
        # superadmin
        $students = User::where('type',1)->get();
        $teachers = User::where('type',7)->get();
        $branchadmin = User::where('type',6)->get();
        $receptions = User::where('type',3)->get();
        $parents = User::where('type',2)->get();
        $superadmin = User::where('type',0)->get();

      }elseif (auth()->user()->type == 1) {
        # telebe
        $students = User::where('type',1)->where('branch_id',$branch_id)->where('group_id',$group_id)->get();
        $teachers = User::where('type',7)->where('branch_id',$branch_id)->where('group_id',$group_id)->get();
        $branchadmin = User::where('type',6)->where('branch_id',$branch_id)->get();
        $receptions = User::where('type',3)->where('branch_id',$branch_id)->get();
        $parents = null;
        $superadmin = User::where('type',0)->get();

      }elseif (auth()->user()->type == 2) {
        # valideyin
        $students = null;
        $teachers = User::where('type',7)->where('branch_id',$branch_id)->where('group_id',$group_id)->get();
        $branchadmin = User::where('type',6)->where('branch_id',$branch_id)->get();
        $receptions = User::where('type',3)->where('branch_id',$branch_id)->get();
        $parents = null;
        $superadmin = User::where('type',0)->get();

      }elseif (auth()->user()->type == 3) {
        # reception
        $students = User::where('type',1)->where('branch_id',$branch_id)->get();
        $teachers = User::where('type',7)->where('branch_id',$branch_id)->get();
        $branchadmin = User::where('type',6)->where('branch_id',$branch_id)->get();
        $receptions = User::where('type',3)->where('branch_id',$branch_id)->get();
        $parents = User::where('type',2)->where('branch_id',$branch_id)->get();
        $superadmin = User::where('type',0)->get();

      }elseif (auth()->user()->type == 6) {
        # rehber
        $students = User::where('type',1)->where('branch_id',$branch_id)->get();
        $teachers = User::where('type',7)->where('branch_id',$branch_id)->get();
        $branchadmin = User::where('type',6)->where('branch_id',$branch_id)->get();
        $receptions = User::where('type',3)->where('branch_id',$branch_id)->get();
        $parents = User::where('type',2)->where('branch_id',$branch_id)->get();
        $superadmin = User::where('type',0)->get();

      }elseif (auth()->user()->type == 7) {
        # muellim
        $students = User::where('type',1)->where('branch_id',$branch_id)->get();
        $teachers = User::where('type',7)->where('branch_id',$branch_id)->get();
        $branchadmin = User::where('type',6)->where('branch_id',$branch_id)->get();
        $receptions = User::where('type',3)->where('branch_id',$branch_id)->get();
        $parents = User::where('type',2)->where('branch_id',$branch_id)->get();
        $superadmin = User::where('type',0)->get();

      }else {
        $students = null;
        $teachers = null;
        $branchadmin = null;
        $receptions = null;
        $parents = null;
        $superadmin = null;
      }
    	return view('compose', compact('students','teachers','branchadmin','receptions','parents','superadmin','in','out'));
    }


    public function insert(Request $request){
      // dd($request);
      foreach ($request->to_users as $to) {

    	$insert = new Chat;
    	$insert->message = $request->message;
    	$insert->from = auth()->user()->id;
    	$insert->to = $to;
    	$insert->status = '0';

      	if ($request->attachment) {
          $fileName = $request->attachment->getClientOriginalName();
          $newName = time().'_'.$fileName;
          $request->attachment->move('message_files/',$newName);
          $insert->attachment = $newName;
      	}
      	else{
      		$insert->attachment=null;
      	}
      	$insert->save();
      }
      	return back();
      }


     public function inbox() {
      $id = auth()->user()->id;
      $in = Chat::where('to',$id)->where('status','0')->get();
      $out = Chat::where('from',$id)->get();
      $chat = Chat::where('to',$id)->get();

    	return view('inbox', compact('in','out','chat'));
    }

     public function sent_mail() {
      $id = auth()->user()->id;
      $in = Chat::where('to',$id)->where('status','0')->get();
      $out = Chat::where('from',$id)->get();
      $chat = Chat::where('from',$id)->get();

    	return view('sent_mail', compact('in','out','chat'));
    }

    public function messages_show($id){

      $userid = auth()->user()->id;
      $in = Chat::where('to',$userid)->where('status','0')->get();
      $out = Chat::where('from',$userid)->get();

    	$chats = Chat::find($id);
    	if ($chats->status!="1" && $chats->from != $userid) {
    	   $chats->status="1";
    	   $chats->save();
    	}


    	return view ('single-message',compact('chats','in','out'));
    }

    public function all_messages()
    {
      if (auth()->user()->type == 0) {
        $data = Chat::orderBy('id','desc')->get();

        return view ('all_messages',compact('data'));
      }else {
        return back();
      }
    }
}
