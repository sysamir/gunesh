<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lesson;
use App\Group;
use App\User;
use App\Ruller;
use App\RullerLists;
use Auth;
use DateTime;
use DateInterval;
use DatePeriod;



class RullerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if ( Auth::user()->type==1 and Auth::user()->type==2) {

        $group_id = Auth::user()->group_id;
        $rullers = Ruller::where('group_id',$group_id)->get();
        return view('/ruller/index',compact('rullers'));

         }
         else{
           $rullers =   Ruller::all();
        return view('/ruller/index',compact('rullers'));

         }

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group = Group::all();
        return view('/ruller/create',compact('lesson','group','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        RullerLists::create([
            'day1' => $request->day1,
            'day2' => $request->day2,
            'day3' => $request->day3,
            'day4' => $request->day4,
            'day5' => $request->day5,
            'day6' => $request->day6,
            'day7' => $request->day7,
            'group_id' => $request->group_id,
        ]);

        $end = new DateTime( '3 week' );
        $interval = DateInterval::createFromDateString('1 week');

        if ($request->day1) {
          $begin1 = new DateTime( 'Monday' );
          $period1 = new DatePeriod($begin1, $interval, $end);
          foreach ($period1 as $c) {
            Ruller::create([
                'group_id' => $request->group_id,
                'date' => $c,
                'time' => $request->day1,
            ]);
          }
        }

        if ($request->day2) {
          $begin2 = new DateTime( 'Tuesday' );
          $period2 = new DatePeriod($begin2, $interval, $end);
          foreach ($period2 as $c) {
            Ruller::create([
                'group_id' => $request->group_id,
                'date' => $c,
                'time' => $request->day2,
            ]);
          }
        }

        if ($request->day3) {
          $begin3 = new DateTime( 'Wednesday' );
          $period3 = new DatePeriod($begin3, $interval, $end);
          foreach ($period3 as $c) {
            Ruller::create([
                'group_id' => $request->group_id,
                'date' => $c,
                'time' => $request->day3,
            ]);
          }
        }

        if ($request->day4) {
          $begin4 = new DateTime( 'Thursday' );
          $period4 = new DatePeriod($begin4, $interval, $end);
          foreach ($period4 as $c) {
            Ruller::create([
                'group_id' => $request->group_id,
                'date' => $c,
                'time' => $request->day4,
            ]);
          }
        }

        if ($request->day5) {
          $begin5 = new DateTime( 'Friday' );
          $period5 = new DatePeriod($begin5, $interval, $end);
          foreach ($period5 as $c) {
            Ruller::create([
                'group_id' => $request->group_id,
                'date' => $c,
                'time' => $request->day5,
            ]);
          }
        }

        if ($request->day6) {
          $begin6 = new DateTime( 'Saturday' );
          $period6 = new DatePeriod($begin6, $interval, $end);
          foreach ($period6 as $c) {
            Ruller::create([
                'group_id' => $request->group_id,
                'date' => $c,
                'time' => $request->day6,
            ]);
          }
        }

        if ($request->day7) {
          $begin7 = new DateTime( 'Sunday' );
          $period7 = new DatePeriod($begin7, $interval, $end);
          foreach ($period7 as $c) {
            Ruller::create([
                'group_id' => $request->group_id,
                'date' => $c,
                'time' => $request->day7,
            ]);
          }
        }


        return back()->with('success', 'Yeni cədvəl əlavə olundu!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ruller = Ruller::find($id);
        $users = User::all();
        $group = Group::all();
        $lesson = Lesson::all();
        return view('ruller.edit',compact('ruller','users','group','lesson'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $ruller = Ruller::find($id);
        $ruller->teacher=$request->teacher;
            $ruller->time=$request->time;
            $ruller->date=$request->date;
            $ruller->subject_id=$request->subject_id;
            $ruller->group_id=$request->group_id;




        $ruller->save();
        return back()->with('success', 'Uğurlu! Cədvəl Redaktə Edildi.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $ruller = Ruller::find($id);

        $ruller->delete();
        return back()->with('delete', 'Uğurlu! Qurup Silindi.');
    }


    public function view()
    {
    $group_id = Auth::user()->group_id;
    $ruller = Ruller::where('group_id',$group_id)->get();
      return view('ruller.view',compact('ruller'));
    }
}
