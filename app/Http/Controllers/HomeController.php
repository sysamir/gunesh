<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Invite;
use App\Students;
use App\Branchadmin;
use App\Branch;
use App\Group;
use App\Parents;
use App\Reseption;


use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function status(Request $request,$id){
      $status = User::find($id);
      $status->status=$request->status;
      $status->save();
      return back();

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->type==2) {
      $id = Auth::user()->id;
      $parent = Parents::where('user_id',$id)->first();
      $invites = Invite::with('parents','students','students_invited')->where('parent_id',Auth::user()->id)->get();

            return view('parents.index',compact('invites'));
         }

      //    if (Auth::user()->type==3) {

      //      $branch_id = Auth::user()->branch_id;
      //      $users = User::where('branch_id',$branch_id)->get();
      //      return view('reseption.index' ,compact('users'));
      //    }
      //     if (Auth::user()->type==6) {

      //      $branch_id = Auth::user()->branch_id;
      //      $users = User::where('branch_id',$branch_id)->get();
      //      return view('branchadmin.index' ,compact('users'));
      //    }
         $students = User::where('type','1')->get();
         $parents = User::where('type','2')->get();
         $reseption = User::where('type','3')->get();
         $branchadmin = User::where('type','6')->get();
         $branch = Branch::all();
         $group = Group::all();
         $load = sys_getloadavg();
         $ram = convert(memory_get_usage(true));

         return view('home',compact('load','ram','students','parents','reseption','branchadmin','branch','group'));
    }
     public function index_attendance($id)
    {
       $branch_id = Auth::user()->branch_id;
           $users = User::where('branch_id',$branch_id)->get();
        return view('teacher.show',compact('users','id'));
    }


}
