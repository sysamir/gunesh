<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Branch;
use App\Branchadmin;
use App\Teacher;
use App\Reseption;
use App\Parents;
use App\Students;
use App\Invite;
use App\Group;



class UserController extends Controller
{

  public function new_register(Request $request)
  {

        $user = new User;
        $user->name=$request->name;
        $user->surname=$request->surname;
        $user->email=$request->email;
        $user->mobile=$request->mobile;
        $user->group_id=$request->group_id;


        if ($request->group_id) {
          $group = Group::where('id',$request->group_id)->first();
          $user->branch_id=$group->branch_id;
        }

        $userfind = User::where('email',$request->email)->first();
        if ($userfind) {
          return back()->with('unsuccess', 'Uğursuz! Belə Bir e-poçt var.');
        }

        $user->type=$request->type;
        $pass=$request->password;
        $user->password=bcrypt($pass);
        $user->save();

        if ($request->type==1) {

        $student = new Students;
            $student->user_id=$user->id;
            $student->kart_id=rand();
            $student->save();
            // return back();

        }

        if ($request->type==2) {

        $parents = new Parents;
            $parents->user_id=$user->id;
            $parents->branch_id=$group->branch_id;
            $parents->save();
        }
        $credentials = array(
          'email' => $request->email,
          'password' => $request->password
        );

        if (Auth::attempt($credentials)) {
          return redirect('home');
        }
  }

public function index(){
  if (Auth::user()->type==0) {

     $users = User::all();

    return view('users.index',compact('users'));

  }




     if (Auth::user()->type==3) {

           $branch_id = Auth::user()->branch_id;
           $users = User::where('branch_id',$branch_id)->get();
           return view('reseption.index' ,compact('users'));
         }


     if (Auth::user()->type==6) {

           $branch_id = Auth::user()->branch_id;
           $users = User::where('branch_id',$branch_id)->get();
           return view('branchadmin.index' ,compact('users'));
         }
       }

    public function create()
    {
  if (Auth::user()->type==0) {


    $branch = Branch::all();

    return view('users.create',compact('branch'));
    }

    if (Auth::user()->type==6) {
    $branch = Branch::all();

           $branch_id = Auth::user()->branch_id;
           $gro = Group::where('branch_id',$branch_id)->get();
    return view('users.create',compact('gro','branch'));



  }

  else{
    return ('Error');
  }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $fileName           =$request->img->getClientOriginalName();
      $newName            =time().'_'.$fileName;
      $request->img->move('uploads/',$newName);

            $user = new User;
            $user->surname=$request->surname;
            $user->name=$request->name;
            $user->email=$request->email;
            $user->mobile=$request->mobile;
            $user->about=$request->about;
            $user->type=$request->type;
            $user->date=$request->date;
            $user->branch_id=$request->branch_id;
            $user->group_id=$request->group_id;
            $pass=$request->password;
            $user->password=bcrypt($pass);
            $user->img=$newName;

            $user->save();

              if ($request->type==6) {
            $branchadmin = new Branchadmin;
            $branchadmin->user_id=$user->id;
            $branchadmin->branch_id=$user->branch_id;
            $branchadmin->save();
            return back()->with('success', 'Uğurlu! Istifadeci Əlavə Olundu.');


            }

            if ($request->type==3) {
            $reseption = new Reseption;
            $reseption->user_id=$user->id;
            $reseption->branch_id=$user->branch_id;
            $reseption->save();
            return back()->with('success', 'Uğurlu! Istifadeci Əlavə Olundu.');



            }

             if ($request->type==7) {
            $teacher = new Teacher;
            $teacher->user_id=$user->id;
            $teacher->group_id=$user->group_id;
            $teacher->branch_id=$user->branch_id;
            $teacher->save();
            return back()->with('success', 'Uğurlu! Istifadeci Əlavə Edildi.');


            }

            if ($request->type==1) {
            $Students = new Students;
            $Students->user_id=$user->id;
            $Students->branch_id=$user->branch_id;
            $Students->group_id=$user->group_id;
            $Students->kart_id=rand();
            $Students->save();
            return back()->with('success', 'Uğurlu! Istifadeci Əlavə Edildi.');



            }
            if ($request->type==2) {
            $Parents = new Parents;
            $Parents->user_id=$user->id;
            $Parents->branch_id=$user->branch_id;
            $Parents->group_id=$user->group_id;
            $Parents->save();
            return back()->with('success', 'Uğurlu! Istifadeci Əlavə Edildi.');



            }


            return back();

    }
      public function edit($id)
    {
            $user = User::find($id);
    $branch = Branch::all();
    $group = Group::all();

     if (Auth::user()->type==6) {
    $branch = Branch::all();

           $branch_id = Auth::user()->branch_id;
           $gro = Group::where('branch_id',$branch_id)->get();
    return view('users.create',compact('gro','branch'));



  }



    return view('users.edit',compact('user','branch','group'));

    }






    public function profile()
    {

        if ( Auth::user()->type==1) {
      $id = Auth::user()->id;


      $student = Students::where('user_id',$id)->first();

      $p = Invite::with('parents')->where('status','0')->where('student_id',$id)->first();
       return view('students.index',compact('parent','student','p'));
    }
        else{
        	return view('profile');
        }
}



public function profile_update(Request $request ){

            $user = Auth::user();
        $pass=$request->password;
        $password_repeat=$request->password_repeat;

         if ($pass==null) {

            $user->surname=$request->surname;
            $user->name=$request->name;
            $user->email=$request->email;
            $user->mobile=$request->mobile;
            $user->about=$request->about;
            $user->date=$request->date;


           if ($request->img!="") {
              $fileName           =$request->img->getClientOriginalName();
              $newName            =time().'_'.$fileName;
              $request->img->move('uploads/',$newName);


  }else{
              $newName            =$user->img;
          }

            $user->img=$newName;
            $user->save();

            return redirect('/profile');

        }



        else
        {
            $user->password=bcrypt($pass);
            $user->surname=$request->surname;
            $user->name=$request->name;
            $user->email=$request->email;
            $user->mobile=$request->mobile;
            $user->about=$request->about;
            $user->date=$request->date;


  if ($request->img!="") {
              $fileName           =$request->img->getClientOriginalName();
              $newName            =time().'_'.$fileName;
              $request->img->move('uploads/',$newName);


  }else{
              $newName            =$user->img;
          }

            $user->img=$newName;
            $user->save();


            $user->save();
            return redirect('/profile');
        }


 }

 public function update(Request $request,$id ){

      $user = User::find($id);
      $pass=$request->password;
      $password_repeat=$request->password_repeat;

      if ($pass==null) {

        $user->surname = $request->surname;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->about = $request->about;
        $user->date = $request->date;
        $user->group_id = $request->group_id;
        $user->branch_id = $request->branch_id;
        $user->type = $request->type;

      if ($request->img!="") {

        $fileName = $request->img->getClientOriginalName();
        $newName = time().'_'.$fileName;
        $request->img->move('uploads/',$newName);

      }else{
        $newName = $user->img;
      }
        $user->img=$newName;
        $user->save();
        return back()->with('success', 'Uğurlu! Məlumatlar Dəyişdirildi.');
  }



        else
        {
            $user->password=bcrypt($pass);
            $user->surname=$request->surname;
            $user->name=$request->name;
            $user->email=$request->email;
            $user->mobile=$request->mobile;
            $user->about=$request->about;
            $user->date=$request->date;
            $user->group_id=$request->group_id;
            $user->branch_id=$request->branch_id;



  if ($request->img!="") {
              $fileName           =$request->img->getClientOriginalName();
              $newName            =time().'_'.$fileName;
              $request->img->move('uploads/',$newName);


  }else{
              $newName            =$user->img;
          }

            $user->img=$newName;
            $user->save();



            return back()->with('success', 'Uğurlu! Məlumatlar Dəyişdirildi.');
        }


 }
 public function destroy($id)
    {
         $user = User::find($id);

        $user->delete();
        return back()->with('delete', 'Uğurlu! Istifadeci Silindi.');
        }

}
