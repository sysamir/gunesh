<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::All();

        return view('branch/index',compact('branches')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('branch.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    
     if ($request->name=="") {

       return back()->with('unsuccess', 'Uğursuz! Filial Adını Daxil Edin.');

        
     }else{
        $branch = new Branch;
        $branch->name=$request->name;
        $branch->save();

        return back()->with('success', 'Uğurlu! Filial Əlavə Edildi.');
        
     }

 }
         
        
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $branches = Branch::All();

        return view('branch/index',compact('branches')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branch::find($id);

        return view('branch/edit',compact('branch')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $branches = Branch::find($id);

        $branches->name=$request->name;

        $branches->save();

        return back()->with('success', 'Uğurlu! Filial Redaktə Edildi.'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $branch = Branch::find($id);

        $branch->delete();
        return back()->with('delete', 'Uğurlu! Filial Silindi.'); 
    }
}
