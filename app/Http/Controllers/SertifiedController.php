<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Sertified;
use App\User;
use File;

class SertifiedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id)
    {
          $serfieds = Sertified::where('user_id',$id)->get();
       
       return view('sertified.create',compact('id','serfieds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $user = User::find($id);

      $fileName           =$request->file->getClientOriginalName();
      $file            =time().'_'.$fileName;
      $request->file->move('sertified/',$file);

        Sertified::create([
              'user_id' => $user->id,
              'group_id' => $user->group_id,
              'title' => $request->title,
              'file' => $file,
            ]);
        return back()->with('success', 'Uğurlu! Sertifikat Əlavə Edildi.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('sertified.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $s = Sertified::find($id);
        $files = public_path('sertified').'/'.$s->file;
        File::delete($files);
        $s->delete();
        return back()->with('delete', 'Uğurlu! Sertifikat Silindi.');
    }
}
