<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Ruller;
use App\Attendance;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
      $users = User::with('attendance')
      ->where('group_id',auth()->user()->group_id)
      ->where('type',1)
      ->get();

      return view('attendance.index',compact('users','id'));
    }

    public function check(Request $request)
    {
      if (count($request->action)>0) {

        foreach ($request->action as $c) {
          $a = Attendance::where('user_id',$c)->where('ruller_id',$request->ruller_id)->first();
          if (count($a)>0) {
            if ($request->status==null) {
            $a->price = $request->price;
            $a->save();
            }
            else{
            $a->status = $request->status;
            $a->save();
          }
          }else {
            Attendance::create([
              'user_id' => $c,
              'group_id' => auth()->user()->group_id,
              'ruller_id' => $request->ruller_id,
              'status' => $request->status,
              'price' => $request->price,
            ]);
          }
        }
      }

      return back();
    }


}
