<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lesson;


class LessonController extends Controller
{
   

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
       public function index()
    {
        $lesson = Lesson::all();
        return view('/lesson/index',compact('lesson'));
    }
    public function create()
    {
        return view('/lesson/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
   public function store(Request $request){

        $lesson = new Lesson;
            $lesson->name=$request->name;

        $lesson->save();
       return back()->with('success', 'Uğurlu! Fenn Əlavə Edildi.');
        
    }
     

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $lesson = Lesson::find($id);

        return view('lesson/edit',compact('lesson'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $lesson = Lesson::find($id);
        
         $lesson->name=$request->name;

        $lesson->save();

        return redirect('/lesson')->with('success', 'Uğurlu! Məlumatlar Dəyişdirildi.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
         $lesson = Lesson::find($id);

        $lesson->delete();
        return back()->with('delete', 'Uğurlu! Fenn Silindi.'); 
        }

}

