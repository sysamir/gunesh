<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','group_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

     public function student()
    {
        return $this->belongsTo('App\Students','id','user_id');
    }
     public function parents()
    {
        return $this->hasMany('App\Parents');
    }
    public function attendance()
    {
        return $this->hasMany('App\Attendance');
    }
     public function sertified()
    {
        return $this->hasMany('App\Sertified');
    }
     public function branch()
    {
        return $this->belongsTo('App\Branch');
    }
}
