<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RullerLists extends Model
{
    protected $table="ruller_lists";

    protected $fillable = [
       'day1',
       'day2',
       'day3',
       'day4',
       'day5',
       'day6',
       'day7',
       'group_id',
    ];

}
