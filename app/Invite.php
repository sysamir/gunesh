<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    protected $table="invite";

    protected $fillable = [
        'parent_id','student_id', 'status',
    ];

       public function parents()
    {
        return $this->belongsTo(User::class,'parent_id');
    }

       public function students()
    {
        return $this->belongsTo(User::class,'student_id');
    }

    public function students_invited()
 {
     return $this->belongsTo(Students::class,'student_id','user_id');
 }
}
